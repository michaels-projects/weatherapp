package com.weather.michael.weatherapp.dictionary;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;


/* Implements a trie. We store the input list of words in tries so
 * that we can efficiently find words with a given prefix. 
 */ 
public class Trie
{
    // The root of this trie.
    TrieNode root;

    public Trie() {
        root = new TrieNode();
    }

    public List<City> completionsForPrefix(String prefix, int limit) {
        TrieNode lastNode = root;
        int i = 0;
        for (i = 0; i < prefix.length(); i++) {
            lastNode = lastNode.getChild(prefix.charAt(i));
            if (lastNode == null) {
                return new LinkedList<>();
            }
        }
        StringBuilder prefixBuilder = new StringBuilder(prefix);
        LinkedList<City> completions = new LinkedList<>();
        prefixCompletionsRec(lastNode, completions, prefixBuilder, limit);
        return completions;
    }
    private void prefixCompletionsRec(TrieNode root, LinkedList<City> completions, StringBuilder prefix, int limit) {
        if(completions.size() >= limit) {
            return;
        }
        if(root.terminates()) {
            completions.add(root.getCity());
        }
        for(Map.Entry<Character, TrieNode> entry : root.getChildren()) {
            prefixCompletionsRec(root.getChild(entry.getKey()), completions, prefix.append(entry.getKey()), limit);
        }
    }


    /* Checks whether this trie contains a string with the prefix passed
     * in as argument.
     */
    public boolean contains(String prefix, boolean exact) {
        TrieNode lastNode = root;
        int i = 0;
        for (i = 0; i < prefix.length(); i++) {
            lastNode = lastNode.getChild(prefix.charAt(i));
            if (lastNode == null) {
                return false;	 
            }
        }
        return !exact || lastNode.terminates();
    }
    
    public boolean contains(String prefix) {
    	return contains(prefix, false);
    }
    
    public TrieNode getRoot() {
    	return root;
    }
}
