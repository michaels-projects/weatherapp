package com.weather.michael.weatherapp.data;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface OpenWeatherService {
    public static final String APP_ID = "686cfc257aa6b3304462bc701fb952e3";

    @GET("/data/2.5/forecast?")
    Call<CityWeather> getWeatherForecast(@Query("id") int id, @Query("APPID") String appId);
}
