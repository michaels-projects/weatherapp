package com.weather.michael.weatherapp.data;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface UserCityDao {
    @Query("SELECT * FROM usercity")
    LiveData<List<UserCity>> loadAll();

    @Query("SELECT * FROM usercity where city_id = :cityId")
    List<UserCity> getById(int cityId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(UserCity... userCities);

}
