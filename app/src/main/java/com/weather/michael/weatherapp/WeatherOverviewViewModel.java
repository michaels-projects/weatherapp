package com.weather.michael.weatherapp;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.weather.michael.weatherapp.data.AppDb;
import com.weather.michael.weatherapp.data.UserCity;
import com.weather.michael.weatherapp.data.UserCityDao;
import com.weather.michael.weatherapp.data.UserCityRepository;

import java.util.List;

public class WeatherOverviewViewModel extends AndroidViewModel {
    Application application;

    public WeatherOverviewViewModel(Application application) {
        super(application);
        this.application = application;
    }

    public LiveData<List<UserCity>> getUserCities() {
        return UserCityRepository.getRepository(application).getCities();
    }
}
