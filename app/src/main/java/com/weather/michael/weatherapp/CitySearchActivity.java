package com.weather.michael.weatherapp;

import android.app.SearchManager;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.weather.michael.weatherapp.data.CityWeather;
import com.weather.michael.weatherapp.data.CompleteWeatherReport;
import com.weather.michael.weatherapp.data.MainWeatherReport;
import com.weather.michael.weatherapp.data.UserCityRepository;
import com.weather.michael.weatherapp.dictionary.City;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CitySearchActivity extends AppCompatActivity {
    private RecyclerView weatherList;
    private WeatherListAdapter weatherListAdapter;
    private CitySearchViewModel citySearchViewModel;

    private CityWeather weather;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city_search);
        Logger.getAnonymousLogger().log(Level.INFO, "Opening CitySearchActivity");

        // Setup the menu bar
        Toolbar menu = (Toolbar) findViewById(R.id.cityWeatherToolbar);
        setSupportActionBar(menu);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        menu.setTitle("City Weather");

        Intent intent = getIntent();
        int cityId = 0;
        if(Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            Uri uri = intent.getData();
            if(uri != null) {
                cityId = Integer.valueOf(uri.toString());
            } else {
                Logger.getAnonymousLogger().log(Level.WARNING, String.format("Query is %s and uri is %s", query, uri));
                return; // side effect of searhc. need to resolve
            }
            Logger.getAnonymousLogger().log(Level.INFO, String.format("Query is %s", query));
        }

        citySearchViewModel = ViewModelProviders.of(this).get(CitySearchViewModel.class);
        citySearchViewModel.setCityId(cityId);

        final City city = new City();
        city.id = cityId;

        UserCityRepository.getRepository(getBaseContext()).addCity(city);
        
        citySearchViewModel.getCity().observe(this, new Observer<CityWeather>() {
            @Override
            public void onChanged(@Nullable CityWeather cityWeather) {
                weatherListAdapter.cityWeather = cityWeather;
                weatherListAdapter.notifyDataSetChanged();
            }
        });

        weatherList = this.findViewById(R.id.weatherList);
        weatherList.setLayoutManager(new LinearLayoutManager(this));
        weatherListAdapter = new WeatherListAdapter();
        weatherList.setAdapter(weatherListAdapter);
    }

    private static class WeatherCell extends RecyclerView.ViewHolder {
        private TextView weatherDay, weatherTime, weatherMinTemp, weatherMaxTemp;

        public WeatherCell(@NonNull View itemView, TextView weatherDay, TextView weatherTime, TextView weatherMinTemp, TextView weatherMaxTemp) {
            super(itemView);
            this.weatherDay = weatherDay;
            this.weatherTime = weatherTime;
            this.weatherMinTemp = weatherMinTemp;
            this.weatherMaxTemp = weatherMaxTemp;
        }

        public WeatherCell(@NonNull View itemView) {
            super(itemView);
        }

        @Override
        public String toString() {
            return super.toString();
        }
    }

    private static class WeatherListAdapter extends RecyclerView.Adapter<WeatherCell> {
        CityWeather cityWeather;
        private static final SimpleDateFormat dayFormat = new SimpleDateFormat("E");
        private static final SimpleDateFormat hourFormat = new SimpleDateFormat("hh");

        @NonNull
        @Override
        public WeatherCell onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cell_weather_detail, viewGroup, false);
            TextView weatherDay = v.findViewById(R.id.weatherDay);
            TextView weatherTime = v.findViewById(R.id.weatherTime);
            TextView weatherMinTemp = v.findViewById(R.id.weatherMinTemp);
            TextView weatherMaxTemp = v.findViewById(R.id.weatherMaxTemp);

            WeatherCell vh = new WeatherCell(v, weatherDay, weatherTime, weatherMinTemp, weatherMaxTemp);

            return vh;
        }

        @Override
        public void onBindViewHolder(@NonNull WeatherCell weatherCell, int i) {
            CompleteWeatherReport completeReport = cityWeather.getList().get(i);
            MainWeatherReport mainWeatherReport = completeReport.getMain();

            Date date = new Date(completeReport.getDt()*1000);
            String day = dayFormat.format(date);
            weatherCell.weatherDay.setText(day);

            String hour = hourFormat.format(date);
            weatherCell.weatherTime.setText(hour);

            weatherCell.weatherMaxTemp.setText(String.format(Locale.US, "%f", mainWeatherReport.getTemp()));
            weatherCell.weatherMinTemp.setText(String.format(Locale.US, "%f", mainWeatherReport.getTemp()));
        }

        @Override
        public int getItemCount() {
            if(cityWeather == null) {
                return 0;
            }
            return cityWeather.getList().size();
        }
    }
}
