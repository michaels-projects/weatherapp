package com.weather.michael.weatherapp.data;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

@Database(entities = {UserCity.class}, version = 1, exportSchema = false)
public abstract class AppDb extends RoomDatabase {
    public abstract UserCityDao userCityDao();

    private static AppDb appDb;
    public static synchronized AppDb getDb(Context context) {
        if(appDb == null) {
            appDb = Room.databaseBuilder(context, AppDb.class, "app-db").build();
        }
        return appDb;
    }
}
