package com.weather.michael.weatherapp;

import android.app.SearchManager;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.CursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.weather.michael.weatherapp.data.UserCity;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class WeatherOverviewActivity extends AppCompatActivity {

    private RecyclerView cityList;
    private CityListAdapter cityListAdapter;
    private RecyclerView.LayoutManager cityListLayoutManager;
    private WeatherOverviewViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_overview);

        // Setup the menu bar
        Toolbar menu = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(menu);

        cityListAdapter = new CityListAdapter();

        cityList = findViewById(R.id.cityList);
        cityList.setAdapter(cityListAdapter);

        cityListLayoutManager = new LinearLayoutManager(this);
        cityList.setLayoutManager(cityListLayoutManager);

        viewModel = ViewModelProviders.of(this).get(WeatherOverviewViewModel.class);
        viewModel.getUserCities().observe(this, new Observer<List<UserCity>>() {
            @Override
            public void onChanged(@Nullable List<UserCity> userCities) {
                cityListAdapter.cities = userCities;
                cityListAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_weather_overview, menu);

        MenuItem searchItem = menu.findItem(R.id.action_menu_search);
        final SearchView searchView = (SearchView) searchItem.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                Logger.getAnonymousLogger().log(Level.INFO, String.format("Query Text: %s", s));
                return false;
            }
        });

//        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
//            @Override
//            public boolean onQueryTextSubmit(String s) {
//                searchView.setQuery("", false);
//                searchView.setIconified(true);
//                return false;
//            }
//
//            @Override
//            public boolean onQueryTextChange(String s) {
//                return false;
//            }
//        });
//        searchView.setOnSuggestionListener(new SearchView.OnSuggestionListener(){
//            @Override
//            public boolean onSuggestionSelect(int i) {
//                return false;
//            }
//
//            @Override
//            public boolean onSuggestionClick(int i) {
//                Object item = searchView.getSuggestionsAdapter().getItem(i);
//                searchView.setQuery(((Cursor)item).getString(1), true);
////                searchView.set
//
//                return true;
//            }
//        });


//        // Associate searchable configuration with the SearchView
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
//        SearchView searchView =
//                (SearchView) menu.findItem(R.layout.activity_city_search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

        return super.onCreateOptionsMenu(menu);
    }

    private static class CityCellHolder extends RecyclerView.ViewHolder {
        TextView mText;
        int mPosition;

        CityCellHolder(View itemView, TextView text) {
            super(itemView);
            this.mText = text;
        }
    }

    private static class CityListAdapter extends RecyclerView.Adapter<CityCellHolder> {
        private List<UserCity> cities;
        @NonNull
        @Override
        public CityCellHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_city_weather_overview, parent, false);
            TextView text = v.findViewById(R.id.cityName);
            CityCellHolder holder = new CityCellHolder(v, text);
            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull CityCellHolder viewHolder, int i) {
            UserCity city = cities.get(i);
            String text = String.format("ID: %d", city.getCityId());
            viewHolder.mText.setText(text);
            viewHolder.mPosition = i;
        }

        @Override
        public int getItemCount() {
            if(cities == null) {
                return 0;
            } else {
                return cities.size();
            }
        }
    }

}
