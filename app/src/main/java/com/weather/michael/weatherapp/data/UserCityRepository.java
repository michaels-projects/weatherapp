package com.weather.michael.weatherapp.data;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;

import com.weather.michael.weatherapp.dictionary.City;

import java.util.List;

public class UserCityRepository {
    private static UserCityRepository repository;

    private UserCityDao dao;
    private HandlerThread saveThread;
    private Handler saveHandler;

    private UserCityRepository(Context context) {
        this.dao = AppDb.getDb(context).userCityDao();
        this.saveThread = new HandlerThread("user-repo-access", HandlerThread.MIN_PRIORITY);
        this.saveThread.start();
        this.saveHandler = new Handler(saveThread.getLooper());
    }

    public static synchronized UserCityRepository getRepository(Context context) {
        if(repository == null) {
            repository = new UserCityRepository(context);
        }
        return repository;
    }

    public LiveData<List<UserCity>> getCities() {
        return dao.loadAll();
    }
    public void addCity(final City city) {
        this.saveHandler.post(new Runnable() {
            @Override
            public void run() {
                UserCity userCity = new UserCity();
                userCity.setCityId(city.id);
                userCity.setCityName(city.name);
                userCity.setCountryName(city.country);
                dao.insertAll(userCity);
            }
        });
    }
}
