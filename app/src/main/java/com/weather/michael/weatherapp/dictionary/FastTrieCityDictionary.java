package com.weather.michael.weatherapp.dictionary;

import com.weather.michael.weatherapp.dictionary.City;
import com.weather.michael.weatherapp.dictionary.CityDictionary;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FastTrieCityDictionary extends CityDictionary {

    Trie dictTrie;
    TrieNode root;
    @Override
    public void initialize() {
        dictTrie = new Trie();
    }

    @Override
    void addCityToDictionary(City city) {
        dictTrie.root.addWord(city, city.name.toLowerCase());
    }

    @Override
    public void dictLoadFailed() {
        Logger.getAnonymousLogger().log(Level.WARNING, "Unable to load dictionary");
    }

    @Override
    public List<City> getSuggestions(String prefix, int count) {
        return dictTrie.completionsForPrefix(prefix.toLowerCase(), count);
    }


}
