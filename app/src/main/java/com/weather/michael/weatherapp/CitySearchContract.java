package com.weather.michael.weatherapp;

import android.app.SearchManager;
import android.net.Uri;
import android.provider.BaseColumns;
import android.widget.SearchView;

public class CitySearchContract {

    public static final String CONTENT_AUTHORITY = "com.michael.weatherapp.cities.provider";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);
    public static final String PATH_CITY_SEARCH = "search_suggest_query/*";

    public static final String[] RESULT_COLUMNS = new String[]{BaseColumns._ID, SearchManager.SUGGEST_COLUMN_TEXT_1, SearchManager.SUGGEST_COLUMN_INTENT_DATA};


}
