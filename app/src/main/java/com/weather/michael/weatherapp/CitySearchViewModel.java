package com.weather.michael.weatherapp;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.weather.michael.weatherapp.data.CityWeather;
import com.weather.michael.weatherapp.data.WeatherRepository;
import com.weather.michael.weatherapp.dictionary.City;

public class CitySearchViewModel extends ViewModel {
    private LiveData<CityWeather> cityWeather;
    int cityId;

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public LiveData<CityWeather> getCity() {
        cityWeather = WeatherRepository.getPersistentStore().cityWeather(cityId);
        return cityWeather;
    }

}
