package com.weather.michael.weatherapp.data;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.util.SparseArray;
import android.util.SparseIntArray;

import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class WeatherRepository {
    private static WeatherRepository store;
    OpenWeatherService service;
    private Retrofit retrofit;
    private SparseArray<MutableLiveData<CityWeather>> weatherReports;

    private WeatherRepository() {
        retrofit = new Retrofit.Builder()
                .baseUrl("https://api.openweathermap.org")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        service = retrofit.create(OpenWeatherService.class);
        weatherReports = new SparseArray<>();
    }
    public static synchronized WeatherRepository getPersistentStore() {
        if(store == null) {
            store = new WeatherRepository();
        }
        return store;
    }

    public LiveData<CityWeather> cityWeather(final int cityId) {
        // first query Room... But that's not set up yet.
        MutableLiveData<CityWeather> weatherData = new MutableLiveData<>();
        weatherReports.append(cityId, weatherData);
        Call<CityWeather> call = service.getWeatherForecast(cityId, OpenWeatherService.APP_ID);
        call.enqueue(new Callback<CityWeather>() {
            @Override
            public void onResponse(Call<CityWeather> call, Response<CityWeather> response) {
                MutableLiveData<CityWeather> weatherLiveData = weatherReports.get(cityId);
                weatherLiveData.postValue(response.body());
            }

            @Override
            public void onFailure(Call<CityWeather> call, Throwable t) {
                // todo... for now we'll silently fail
                Logger.getAnonymousLogger().log(Level.WARNING, "Unable to fetch weather data");
            }
        });
        return weatherData;
    }

}
