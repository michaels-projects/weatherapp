package com.weather.michael.weatherapp;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.weather.michael.weatherapp.dictionary.City;
import com.weather.michael.weatherapp.dictionary.CityDictionary;
import com.weather.michael.weatherapp.dictionary.FastTrieCityDictionary;

import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CitySearchContentProvider extends ContentProvider {
    @Override
    public boolean onCreate() {
        dictionary = new FastTrieCityDictionary();
        dictionary.initialize();
        return false;
    }

    private static final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        uriMatcher.addURI(CitySearchContract.CONTENT_AUTHORITY, CitySearchContract.PATH_CITY_SEARCH, 1);
    }
    CityDictionary dictionary;


    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String setOrder) {
        Cursor c = null;
        switch(uriMatcher.match(uri)) {
            case 1:
                dictionary.loadDictionary(R.raw.city_list_short, getContext());
                String query = uri.getLastPathSegment().toLowerCase();
                String lengthStr = uri.getQueryParameter("limit");

                int maxLen = Integer.valueOf(lengthStr);
                if(maxLen == 0) {
                    maxLen = 50;
                }

                List<City> cities = dictionary.getSuggestions(query, maxLen);
//                List<String> cities = new LinkedList<>();
//                cities.add("Test city");
                Logger.getAnonymousLogger().log(Level.INFO, String.format("query is %s and limit is %d", query, maxLen));

                MatrixCursor mc = new MatrixCursor(CitySearchContract.RESULT_COLUMNS, maxLen);
                int i = 0;
                for(City city : cities) {
                    mc.addRow(new Object[]{i++, city.name, city.id});
                }
                c = mc;
        }

        return c;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues contentValues) {
        return null;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String s, @Nullable String[] strings) {
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues contentValues, @Nullable String s, @Nullable String[] strings) {
        return 0;
    }
}
