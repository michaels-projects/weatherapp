package com.weather.michael.weatherapp.data;

public class CompleteWeatherReport {
    int dt;
    MainWeatherReport main;

    public int getDt() {
        return dt;
    }

    public void setDt(int dt) {
        this.dt = dt;
    }

    public MainWeatherReport getMain() {
        return main;
    }

    public void setMain(MainWeatherReport main) {
        this.main = main;
    }
}
