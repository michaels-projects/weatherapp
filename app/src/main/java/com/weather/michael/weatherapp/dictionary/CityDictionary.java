package com.weather.michael.weatherapp.dictionary;

import android.content.Context;
import android.util.JsonReader;

import com.weather.michael.weatherapp.R;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class CityDictionary {
    private Boolean isDictLoaded = false;

    private City parseCityEntry(JsonReader reader) throws IOException {
        City city = new City();

        reader.beginObject();
        while(reader.hasNext()) {
            String name = reader.nextName();
            if(name.equals("id")) {
                city.id = reader.nextInt();
            } else if(name.equals("name")) {
                city.name = reader.nextString();
            } else if(name.equals("country")) {
                city.country = reader.nextString();
            } else {
                reader.skipValue();
            }
//                if(name.equals("coord")) {
//                reader.beginObject();
//                while(reader.hasNext()) {
//                    reader.skipValue();
//                }
//            }
        }
        reader.endObject();
//        Logger.getAnonymousLogger().log(Level.INFO, String.format("parsed city: %s", city.name));
        return city;
    }
    private void buildDictionary(JsonReader reader) throws IOException {
        reader.beginArray();
        while(reader.hasNext()) {
            City city = parseCityEntry(reader);
            addCityToDictionary(city);
        }
    }
    // Takes a resource ID
    public synchronized void loadDictionary(int id, Context context) {
        if (!isDictLoaded) {
            InputStream is = null;
            try {
                Logger.getAnonymousLogger().log(Level.INFO, String.format("instantiating: trie"));
                is = context.getResources().openRawResource(R.raw.city_list_short);

                JsonReader reader = new JsonReader(new InputStreamReader(is, "UTF-8"));
                buildDictionary(reader);
                isDictLoaded = true;
            } catch (IOException e) {
                Logger.getAnonymousLogger().log(Level.WARNING, String.format("unable to open trie file"));
                isDictLoaded = false;
            } finally {
                if (is != null) {
                    try {
                        is.close();
                    } catch (IOException e) { }
                }
            }
        } else {
            Logger.getAnonymousLogger().log(Level.INFO, String.format("retrieved: trie"));
        }
    }

    abstract void addCityToDictionary(City city);
    public abstract void dictLoadFailed();
    public abstract List<City> getSuggestions(String prefix, int count);
    public abstract void initialize();
}
