package com.weather.michael.weatherapp.data;

import com.weather.michael.weatherapp.dictionary.City;

import java.util.List;

public class CityWeather {
    int cnt;
    List<CompleteWeatherReport> list;
    City city;

    public int getCnt() {
        return cnt;
    }

    public void setCnt(int cnt) {
        this.cnt = cnt;
    }

    public List<CompleteWeatherReport> getList() {
        return list;
    }

    public void setList(List<CompleteWeatherReport> list) {
        this.list = list;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }
}
